-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2020 a las 12:52:28
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `videojuegosbd`
--
DROP DATABASE IF EXISTS `videojuegosbd`;
CREATE DATABASE IF NOT EXISTS `videojuegosbd` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `videojuegosbd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memoria_ram`
--

DROP TABLE IF EXISTS `memoria_ram`;
CREATE TABLE `memoria_ram` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `preciototal` int(11) DEFAULT NULL,
  `usuario_id` varchar(255) DEFAULT NULL,
  `listaproductos` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesador`
--

DROP TABLE IF EXISTS `procesador`;
CREATE TABLE `procesador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `caracteristicas` text DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sistema_operativo`
--

DROP TABLE IF EXISTS `sistema_operativo`;
CREATE TABLE `sistema_operativo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta_video`
--

DROP TABLE IF EXISTS `tarjeta_video`;
CREATE TABLE `tarjeta_video` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_entity`
--

DROP TABLE IF EXISTS `user_entity`;
CREATE TABLE `user_entity` (
  `id` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordsinencriptar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_entity_roles`
--

DROP TABLE IF EXISTS `user_entity_roles`;
CREATE TABLE `user_entity_roles` (
  `user_entity_id` varchar(255) NOT NULL,
  `roles` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegos`
--

DROP TABLE IF EXISTS `videojuegos`;
CREATE TABLE `videojuegos` (
  `id` int(11) NOT NULL,
  `desarrollador` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `fechasalida` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `idioma` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `njugadores` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `precio` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegospc`
--

DROP TABLE IF EXISTS `videojuegospc`;
CREATE TABLE `videojuegospc` (
  `id` int(11) NOT NULL,
  `desarrollador` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `fechasalida` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `idioma` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `njugadores` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `precio` varchar(255) DEFAULT NULL,
  `requisitorecomendado_graficos_id` int(11) DEFAULT NULL,
  `requisitorecomendado_memoria_id` int(11) DEFAULT NULL,
  `requisitorecomendado_procesador_id` int(11) DEFAULT NULL,
  `requisitorecomendado_sistema_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegosps4`
--

DROP TABLE IF EXISTS `videojuegosps4`;
CREATE TABLE `videojuegosps4` (
  `id` int(11) NOT NULL,
  `desarrollador` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `fechasalida` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `idioma` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `njugadores` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `precio` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegosxone`
--

DROP TABLE IF EXISTS `videojuegosxone`;
CREATE TABLE `videojuegosxone` (
  `id` int(11) NOT NULL,
  `desarrollador` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `fechasalida` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `idioma` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `njugadores` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `precio` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuegos_esperados`
--

DROP TABLE IF EXISTS `videojuegos_esperados`;
CREATE TABLE `videojuegos_esperados` (
  `id` int(11) NOT NULL,
  `desarrollador` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `fechasalida` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `idioma` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `informacion` text DEFAULT NULL,
  `njugadores` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `voto`
--

DROP TABLE IF EXISTS `voto`;
CREATE TABLE `voto` (
  `valorvoto` int(11) DEFAULT NULL,
  `videojuego_id` int(11) NOT NULL,
  `usuario_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votopc`
--

DROP TABLE IF EXISTS `votopc`;
CREATE TABLE `votopc` (
  `valorvoto` int(11) DEFAULT NULL,
  `videojuegopc_id` int(11) NOT NULL,
  `usuario_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votops4`
--

DROP TABLE IF EXISTS `votops4`;
CREATE TABLE `votops4` (
  `valorvoto` int(11) DEFAULT NULL,
  `videojuegops4_id` int(11) NOT NULL,
  `usuario_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votoxone`
--

DROP TABLE IF EXISTS `votoxone`;
CREATE TABLE `votoxone` (
  `valorvoto` int(11) DEFAULT NULL,
  `videojuegoxone_id` int(11) NOT NULL,
  `usuario_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memoria_ram`
--
ALTER TABLE `memoria_ram`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKki69o5mddbn9qjhybmm339ryo` (`usuario_id`);

--
-- Indices de la tabla `procesador`
--
ALTER TABLE `procesador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sistema_operativo`
--
ALTER TABLE `sistema_operativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarjeta_video`
--
ALTER TABLE `tarjeta_video`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_entity`
--
ALTER TABLE `user_entity`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_entity_roles`
--
ALTER TABLE `user_entity_roles`
  ADD KEY `FKjvvinok3stf32dvgie3vr73s0` (`user_entity_id`);

--
-- Indices de la tabla `videojuegos`
--
ALTER TABLE `videojuegos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videojuegospc`
--
ALTER TABLE `videojuegospc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKs1brgtmy6ak7xydjfvm9h3a3g` (`requisitorecomendado_graficos_id`),
  ADD KEY `FK78rnjhv2jvxogqdmw18y552ad` (`requisitorecomendado_memoria_id`),
  ADD KEY `FKlhs1ltklh7mque205vtabn6ni` (`requisitorecomendado_procesador_id`),
  ADD KEY `FKadu38rc4jb1k3um30u3v626f8` (`requisitorecomendado_sistema_id`);

--
-- Indices de la tabla `videojuegosps4`
--
ALTER TABLE `videojuegosps4`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videojuegosxone`
--
ALTER TABLE `videojuegosxone`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videojuegos_esperados`
--
ALTER TABLE `videojuegos_esperados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `voto`
--
ALTER TABLE `voto`
  ADD PRIMARY KEY (`usuario_id`,`videojuego_id`),
  ADD KEY `FK2orag0ufeuu926m1se1wetx22` (`videojuego_id`);

--
-- Indices de la tabla `votopc`
--
ALTER TABLE `votopc`
  ADD PRIMARY KEY (`usuario_id`,`videojuegopc_id`),
  ADD KEY `FKp6lvg7bk9t3dkgtl586bunkeo` (`videojuegopc_id`);

--
-- Indices de la tabla `votops4`
--
ALTER TABLE `votops4`
  ADD PRIMARY KEY (`usuario_id`,`videojuegops4_id`),
  ADD KEY `FK74oq2w3ganui3kew0k7jqws8v` (`videojuegops4_id`);

--
-- Indices de la tabla `votoxone`
--
ALTER TABLE `votoxone`
  ADD PRIMARY KEY (`usuario_id`,`videojuegoxone_id`),
  ADD KEY `FK81ts201s4dkxab8q8rdf1285n` (`videojuegoxone_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `FKki69o5mddbn9qjhybmm339ryo` FOREIGN KEY (`usuario_id`) REFERENCES `user_entity` (`id`);

--
-- Filtros para la tabla `user_entity_roles`
--
ALTER TABLE `user_entity_roles`
  ADD CONSTRAINT `FKjvvinok3stf32dvgie3vr73s0` FOREIGN KEY (`user_entity_id`) REFERENCES `user_entity` (`id`);

--
-- Filtros para la tabla `videojuegospc`
--
ALTER TABLE `videojuegospc`
  ADD CONSTRAINT `FK78rnjhv2jvxogqdmw18y552ad` FOREIGN KEY (`requisitorecomendado_memoria_id`) REFERENCES `memoria_ram` (`id`),
  ADD CONSTRAINT `FKadu38rc4jb1k3um30u3v626f8` FOREIGN KEY (`requisitorecomendado_sistema_id`) REFERENCES `sistema_operativo` (`id`),
  ADD CONSTRAINT `FKlhs1ltklh7mque205vtabn6ni` FOREIGN KEY (`requisitorecomendado_procesador_id`) REFERENCES `procesador` (`id`),
  ADD CONSTRAINT `FKs1brgtmy6ak7xydjfvm9h3a3g` FOREIGN KEY (`requisitorecomendado_graficos_id`) REFERENCES `tarjeta_video` (`id`);

--
-- Filtros para la tabla `voto`
--
ALTER TABLE `voto`
  ADD CONSTRAINT `FK2orag0ufeuu926m1se1wetx22` FOREIGN KEY (`videojuego_id`) REFERENCES `videojuegos` (`id`),
  ADD CONSTRAINT `FK7e1i10vrme8hxg1mxl7nde542` FOREIGN KEY (`usuario_id`) REFERENCES `user_entity` (`id`);

--
-- Filtros para la tabla `votopc`
--
ALTER TABLE `votopc`
  ADD CONSTRAINT `FK7bu0oc50fmsk6ep6k0jtveqfi` FOREIGN KEY (`usuario_id`) REFERENCES `user_entity` (`id`),
  ADD CONSTRAINT `FKp6lvg7bk9t3dkgtl586bunkeo` FOREIGN KEY (`videojuegopc_id`) REFERENCES `videojuegospc` (`id`);

--
-- Filtros para la tabla `votops4`
--
ALTER TABLE `votops4`
  ADD CONSTRAINT `FK74oq2w3ganui3kew0k7jqws8v` FOREIGN KEY (`videojuegops4_id`) REFERENCES `videojuegosps4` (`id`),
  ADD CONSTRAINT `FKj0xtitqamlassx0bgnh072wu3` FOREIGN KEY (`usuario_id`) REFERENCES `user_entity` (`id`);

--
-- Filtros para la tabla `votoxone`
--
ALTER TABLE `votoxone`
  ADD CONSTRAINT `FK81ts201s4dkxab8q8rdf1285n` FOREIGN KEY (`videojuegoxone_id`) REFERENCES `videojuegosxone` (`id`),
  ADD CONSTRAINT `FKs27qawsenlspdaxw8bptlifnx` FOREIGN KEY (`usuario_id`) REFERENCES `user_entity` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
