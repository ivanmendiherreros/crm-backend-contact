package psp.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	

//    @Bean
//    CorsFilter corsFilter() {
//        CorsFilter filter = new CorsFilter();
//        return filter;
//    }

	
    @Autowired
    private CustomBasicAuthenticationEntryPoint customBasicAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
   
    
    
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { 
        http.httpBasic()
                    
       // .addFilterBefore(corsFilter(), SessionManagementFilter.class) //adds your custom CorsFilter

        .authenticationEntryPoint(customBasicAuthenticationEntryPoint)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, "/empleados").permitAll()
//                    .antMatchers(HttpMethod.POST, "/user/**").permitAll()    
//                    .antMatchers(HttpMethod.GET, "/user/**").permitAll() 
//                //    .antMatchers(HttpMethod.GET, "/users/**").hasRole("ADMIN")     
//                  //  .antMatchers(HttpMethod.GET, "/users/**").hasRole("USERS") 
//                    .antMatchers(HttpMethod.GET, "/users/").permitAll() 
//                    .antMatchers(HttpMethod.GET, "/users").permitAll() 
//
//                    
//                 
//                    .antMatchers(HttpMethod.GET, "/videojuegos").permitAll()
//                    
//                 // 	.antMatchers(HttpMethod.GET, "/videojuegos").hasRole("ADMIN")
//                  //  .antMatchers(HttpMethod.GET, "/videojuego/**").hasRole("ADMIN")
//                  	
//                  	
//               //   	.antMatchers(HttpMethod.GET, "/videojuegos").hasRole("USER")
//                    .antMatchers(HttpMethod.GET, "/videojuego/**").permitAll()
//                   
//                    
//                    
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/videojuegos2").permitAll()
//                 //   .antMatchers(HttpMethod.DELETE, "/videojuego/**").permitAll()
//                     .antMatchers(HttpMethod.DELETE, "/videojuego/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.POST, "/videojuego/**").hasRole("ADMIN")  
//                    .antMatchers(HttpMethod.PUT, "/videojuego/**").hasRole("ADMIN") 
//            //        .antMatchers(HttpMethod.POST, "/videojuego/**").permitAll() 
//          //        .antMatchers(HttpMethod.DELETE, "/videojuego/**").permitAll() 
//              //      .antMatchers(HttpMethod.PUT, "/videojuego/**").permitAll() 
//                
//                    
//                    .antMatchers(HttpMethod.GET, "/videojuegosPC").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuegos2").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuegoPC/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/videojuegoPC/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.DELETE, "/videojuegoPC/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.PUT, "/videojuegoPC/**").hasRole("ADMIN")
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/videojuegosXONE").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuegoXONE/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/videojuegoXONE/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.DELETE, "/videojuegoXONE/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.PUT, "/videojuegoXONE/**").hasRole("ADMIN")
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/videojuegosPS4").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuegoPS4/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/videojuegoPS4/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.DELETE, "/videojuegoPS4/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.PUT, "/videojuegoPS4/**").hasRole("ADMIN")
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/videojuegosEsperados").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuegoEsperado/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/videojuegoEsperado/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.DELETE, "/videojuegoEsperado/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.PUT, "/videojuegoEsperado/**").hasRole("ADMIN")
//                    
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/productos").permitAll()
//                    .antMatchers(HttpMethod.GET, "/producto/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/producto/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.DELETE, "/producto/**").hasRole("ADMIN")
//                    .antMatchers(HttpMethod.PUT, "/producto/**").hasRole("ADMIN")
//                    
//                
//                    .antMatchers(HttpMethod.GET, "/pedidos").permitAll()
//                    .antMatchers(HttpMethod.GET, "/pedido/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.POST, "/pedido/**").permitAll()
//                    .antMatchers(HttpMethod.DELETE, "/pedido/**").permitAll()
//                    .antMatchers(HttpMethod.PUT, "/pedido/**").permitAll()
//                    
//                    .antMatchers(HttpMethod.GET, "/votos").permitAll()
//                    .antMatchers(HttpMethod.GET, "/voto/**").permitAll()
//                   // .antMatchers(HttpMethod.GET, "/videojuegos2").permitAll()
//                    .antMatchers(HttpMethod.POST, "/voto/**").permitAll() 
//                    .antMatchers(HttpMethod.DELETE, "/voto/**").permitAll() 
//                    .antMatchers(HttpMethod.PUT, "/voto/**").permitAll() 
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/votosPC").permitAll()
//                    .antMatchers(HttpMethod.GET, "/votoPC/**").permitAll()
//                    .antMatchers(HttpMethod.POST, "/votoPC").permitAll()
//                    .antMatchers(HttpMethod.DELETE, "/votoPC/**").permitAll() 
//                    .antMatchers(HttpMethod.PUT, "/votoPC/**").permitAll() 
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/votosPS4").permitAll()
//                    .antMatchers(HttpMethod.GET, "/votoPS4/**").permitAll()
//                    .antMatchers(HttpMethod.POST, "/votoPS4").permitAll()
//                    .antMatchers(HttpMethod.DELETE, "/votoPS4/**").permitAll() 
//                    .antMatchers(HttpMethod.PUT, "/votoPS4/**").permitAll() 
//                    
//                    
//                    
//                    .antMatchers(HttpMethod.GET, "/votosXONE").permitAll()
//                    .antMatchers(HttpMethod.GET, "/votoXONE/**").permitAll()
//                    .antMatchers(HttpMethod.POST, "/votoXONE").permitAll()
//                    .antMatchers(HttpMethod.DELETE, "/votoXONE/**").permitAll() 
//                    .antMatchers(HttpMethod.PUT, "/votoXONE/**").permitAll()
//                    
//                    
                    
                    
                    
//                    .antMatchers(HttpMethod.GET, "/videojuegos_Voto").permitAll()
//                    .antMatchers(HttpMethod.GET, "/videojuego_Voto").permitAll()
//                    .antMatchers(HttpMethod.POST, "/videojuego_Voto/**").permitAll() 
//                    .antMatchers(HttpMethod.DELETE, "/videojuego_Voto/**").permitAll() 
//                    .antMatchers(HttpMethod.PUT, "/videojuego_Voto/**").permitAll() 
                    
//                .antMatchers(HttpMethod.GET, "/jugador/**").hasRole("USER")    
//                .antMatchers(HttpMethod.POST, "/jugador/**").hasRole("ADMIN")   
//                .antMatchers(HttpMethod.DELETE, "/jugador/**").hasRole("ADMIN") 
//                .antMatchers(HttpMethod.PUT, "/jugador/**").hasRole("ADMIN")     
          
            
                .antMatchers("/login*").permitAll()                                                     
                .anyRequest().permitAll()
      .and()
      .csrf().disable();    
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
