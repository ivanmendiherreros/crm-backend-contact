package psp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class InputEmpleadoDTO {

	private String nombre;
	private String correo;
	private String telefono;
	private String propietariocontacto;
	private String empresaasociada;
	private String fechaultimaactividad;
	private String estadolealtad;
	private String fechacreacion;
	
}
