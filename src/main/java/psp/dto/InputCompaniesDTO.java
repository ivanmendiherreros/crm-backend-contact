package psp.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class InputCompaniesDTO {


	private String Cif;
	
	
	
	
	private String Nombre;
	
	
	
	private String Actividad_cnae;
	
	
	
	private String Actividad_sic;
	
	
	
	
	private int Empleados;
	
	
	
	
	private String Ventas;
	
	
	
	private int Ebitda;
	
	
	private String Ejercicio_Balance;
	
	
	
	private int Telefono;
	
	
	private String Contacto;
	
	
	private Date Fecha_Descarga;

	
	private String Sector;
	
	
	private String Tipo;
	
	
	
	private String Origen;
	
	
	private String Robot_cargo;
	
	
	
	private String propietario_registroEmpresa;
	
	
	
	private Date fecha_creacion;
	
	
	private Date fecha_ultimaActividad;
	
	
	private String ciudad;
	
	
	private String pais;
	
	
	private String industria;
	
	
	private String nombre_dominioEmpresa;
	
	
	
	private String Descripcion;
	
	
	
	
	private String propietario_contacto;
	
	
	
	private String car;
	
	
	
	private String subcar;
	
	
	
	private String clasificacion_empresa;
	
	
	private String estado_lealtad;
	
	
	private String etapa_ciclovida;
	
	
	private String tipo;
	
	
	private String propietario_registroempresa;
	

}

	
	
