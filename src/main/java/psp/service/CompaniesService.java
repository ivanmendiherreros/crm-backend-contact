package psp.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import psp.model.Companies;
import psp.model.Contactos;
import psp.model.Empleados;
import psp.repository.CompaniesRepository;
import psp.repository.ContactosRepository;

@Service
public class CompaniesService extends BaseService<Companies, Integer, CompaniesRepository>{
	
}