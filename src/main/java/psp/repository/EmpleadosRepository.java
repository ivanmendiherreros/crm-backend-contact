package psp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import psp.model.Empleados;

@Repository
public interface EmpleadosRepository extends JpaRepository<Empleados, Integer> {
}

