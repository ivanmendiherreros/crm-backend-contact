package psp.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;





import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import io.swagger.annotations.ApiResponse;
import psp.model.Contactos;
import psp.model.Empleados;
import psp.service.ContactosService;
import psp.service.EmpleadosService;



@AllArgsConstructor
@RestController
public class ContactosController {


		@Autowired
		private ContactosService jr;
		
		// Devuelve lista completa de games
			@ApiOperation(value="Obtener lista de empleados", notes="Se devuelve una lista")
			@ApiResponses(value= {
					@ApiResponse(code=200, message="OK"),
					@ApiResponse(code=204, message="Los empleados se han obtenido correctamente"),
					@ApiResponse(code=401, message="Sin autorizacion"),
					@ApiResponse(code=403, message="No autorizado"),
					@ApiResponse(code=404, message="Error")
			})
			@GetMapping(value="contactos")
			public List<Contactos> getContactos(){
		            return jr.findAll();
			}
			
		
			
			
			
			
			
			// Devuelve game con id {id}
			@ApiOperation(value="Obtener empleados en funcion de su id")
			@ApiResponses(value= {
					@ApiResponse(code=200, message="OK"),
					@ApiResponse(code=204, message="El empleado se ha obtenido correctamente"),
					@ApiResponse(code=401, message="Sin autorizacion"),
					@ApiResponse(code=403, message="No autorizado"),
					@ApiResponse(code=404, message="El empleado no existe")
			})
			@GetMapping(value="contacto/{id}")
			public ResponseEntity<Contactos> getContacto(@PathVariable int id) {
				Optional<Contactos> empleado = jr.findById(id);
				if (empleado.isPresent()) {
					return ResponseEntity.status(HttpStatus.OK).body(empleado.get());
				}else {
					return ResponseEntity.status(HttpStatus.CONFLICT).build();
				}
			}
			
		

			
			
		
		//	 Añade un game
			@ApiOperation(value="Añadir empleado")
			@ApiResponses(value= {
					@ApiResponse(code=200, message="OK"),
					@ApiResponse(code=201, message="El empleado se ha añadido correctamente"),
					@ApiResponse(code=204, message="El empleado se ha añadido correctamente y te devuelve un cuerpo"),
					@ApiResponse(code=401, message="Sin autorizacion"),
					@ApiResponse(code=403, message="No autorizado"),
				@ApiResponse(code=404, message="El empleado no existe")
			})
			@PostMapping(value="contacto")
			public ResponseEntity<Contactos> postEmpleado(@RequestBody Contactos nuevoempleado) {
				jr.save(nuevoempleado);
				return ResponseEntity.status(HttpStatus.CREATED).body(jr.findById(nuevoempleado.getId()).get());
			}
			
			// Modifica un game con id {id}
//			@ApiOperation(value="Modificar empleado")
//			@ApiResponses(value= {
//					@ApiResponse(code=200, message="OK"),
//					@ApiResponse(code=201, message="El empleado se ha modificado correctamente"),
//				@ApiResponse(code=204, message="El empleado se ha modificado correctamente y te devuelve un cuerpo"),
//				@ApiResponse(code=401, message="Sin autorizacion"),
//				@ApiResponse(code=403, message="No autorizado"),
//				@ApiResponse(code=404, message="El empleado no existe")
//		})
//		@PutMapping(value="Empleado")
//		public ResponseEntity<Empleados> putVideojuego(@RequestBody Empleados nuevoempleado) {
//			//jr.delete(nuevojuego);
//				jr.save(nuevoempleado);
//			return ResponseEntity.status(HttpStatus.CREATED).body(jr.findById(nuevoempleado.getId()).get());
//		}
			
			// Elimina un game con id{id}
			@ApiOperation(value="Eliminar empleado")
			@ApiResponses(value= {
					@ApiResponse(code=200, message="OK"),
					@ApiResponse(code=204, message="El empleado se ha eliminado correctamente y te devuelve un cuerpo"),
					@ApiResponse(code=401, message="Sin autorizacion"),
					@ApiResponse(code=403, message="No autorizado"),
					@ApiResponse(code=404, message="El empleado no existe")
			})
			@DeleteMapping(value="contacto/{id}")
			public ResponseEntity<Empleados>  deleteEmpleados(@PathVariable int id) {
				jr.delete(jr.findById(id).get());
				return ResponseEntity.noContent().build();
			}
		
	}




