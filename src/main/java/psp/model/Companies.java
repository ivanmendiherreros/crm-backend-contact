package psp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.annotations.GenericGenerator;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Companies {

	
//	@ApiModelProperty(value = "id del contacto", dataType="long", position = 1, example="99")
//	@XmlElement(name = "id")
//	@Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
	
	  @Id
	  @GeneratedValue(generator = "id")
	  @GenericGenerator(name = "id", strategy = "id2")
	private int id;
	
	
	
	
	private String Cif;
	
	
	
	
	private String Nombre;
	
	
	
	private String Actividad_cnae;
	
	
	
	private String Actividad_sic;
	
	
	
	
	private int Empleados;
	
	
	
	
	private String Ventas;
	
	
	
	private int Ebitda;
	
	
	private String Ejercicio_Balance;
	
	
	
	private int Telefono;
	
	
	private String Contacto;
	
	
	private Date Fecha_Descarga;

	
	private String Sector;
	
	
	private String Tipo;
	
	
	
	private String Origen;
	
	
	private String Robot_cargo;
	
	
	
	private String propietario_registroEmpresa;
	
	
	
	private Date fecha_creacion;
	
	
	private Date fecha_ultimaActividad;
	
	
	private String ciudad;
	
	
	private String pais;
	
	
	private String industria;
	
	
	private String nombre_dominioEmpresa;
	
	
	
	private String Descripcion;
	
	
	
	
	private String propietario_contacto;
	
	
	
	private String car;
	
	
	
	private String subcar;
	
	
	
	private String clasificacion_empresa;
	
	
	private String estado_lealtad;
	
	
	private String etapa_ciclovida;
	
	
	private String tipo;
	
	
	private String propietario_registroempresa;
	
	
	
	
	
	
}
