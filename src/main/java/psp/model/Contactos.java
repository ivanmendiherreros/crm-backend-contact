package psp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Contactos {

	@ApiModelProperty(value = "id del contacto", dataType="long", position = 1, example="99")
	@XmlElement(name = "id")
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ApiModelProperty(value = "nombre del contacto", dataType="String", position = 2, example="Pepe")
	@XmlElement(name = "nombre")
	private String nombre;
	
	
	@ApiModelProperty(value = "correo del contacto", dataType="String", position = 3, example="Perez")
	@XmlElement(name = "correo")
	private String correo;
	
	
	
	@ApiModelProperty(value ="telefono del contacto", dataType="String", position = 4, example="urlempleado")
	@XmlElement(name = "telefono") 
	private String telefono;

	
	
	@ApiModelProperty(value ="propietario", dataType="String", position = 5, example="urlempleado")
	@XmlElement(name = "propietario") 
	private String propietariocontacto;
	
	
	
	@ApiModelProperty(value ="empresaasociada", dataType="String", position = 6, example="urlempleado")
	@XmlElement(name = "empresaasociada") 

	private String empresaasociada;
	

	@ApiModelProperty(value ="fechaultimaactividad", dataType="String", position = 7, example="urlempleado")
	@XmlElement(name = "fechaultimaactividad") 
	private String fechaultimaactividad;
	
	
	
	@ApiModelProperty(value ="estadolealtad", dataType="String", position = 8, example="urlempleado")
	@XmlElement(name = "estadolealtad") 
	private String estadolealtad;
	
	
	
	
	@ApiModelProperty(value ="fechacreacion", dataType="String", position = 9, example="urlempleado")
	@XmlElement(name = "fechacreacion") 
	private String fechacreacion;




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getNombre() {
		return nombre;
	}




	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getCorreo() {
		return correo;
	}




	public void setCorreo(String correo) {
		this.correo = correo;
	}




	public String getTelefono() {
		return telefono;
	}




	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}




	public String getPropietariocontacto() {
		return propietariocontacto;
	}




	public void setPropietariocontacto(String propietariocontacto) {
		this.propietariocontacto = propietariocontacto;
	}




	public String getEmpresaasociada() {
		return empresaasociada;
	}




	public void setEmpresaasociada(String empresaasociada) {
		this.empresaasociada = empresaasociada;
	}




	public String getFechaultimaactividad() {
		return fechaultimaactividad;
	}




	public void setFechaultimaactividad(String fechaultimaactividad) {
		this.fechaultimaactividad = fechaultimaactividad;
	}




	public String getEstadolealtad() {
		return estadolealtad;
	}




	public void setEstadolealtad(String estadolealtad) {
		this.estadolealtad = estadolealtad;
	}




	public String getFechacreacion() {
		return fechacreacion;
	}




	public void setFechacreacion(String fechacreacion) {
		this.fechacreacion = fechacreacion;
	}




	public Contactos(int id, String nombre, String correo, String telefono, String propietariocontacto,
			String empresaasociada, String fechaultimaactividad, String estadolealtad, String fechacreacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.correo = correo;
		this.telefono = telefono;
		this.propietariocontacto = propietariocontacto;
		this.empresaasociada = empresaasociada;
		this.fechaultimaactividad = fechaultimaactividad;
		this.estadolealtad = estadolealtad;
		this.fechacreacion = fechacreacion;
	}

	public Contactos() {
		
	}
}
